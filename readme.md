particle-vm
==================

Build scripts to create a VM usable with particle io boards.

It will contain cross compilation stuff and the particle cli.

This is based on [local-build](https://docs.particle.io/support/particle-tools-faq/local-build/) from the docs.



usage
-------

Prequisites:

* Vagrant must be properly installed and configured.


1. Run `vagrant up`

    This spins up and configrues the virtual machine.

2. Run `vagrant ssh`

    To access the command line.

3. Copy the .c files to `./share` to make them accessible in the vagrant box

4. Go to the share folder in the VM, ie. `cd /vagrant`

5. run make or whatver yo udo to  build stuff.



Building stuff

building using gcc
-------------------------

we use cpp file not ino files.

As described [here](https://docs.particle.io/reference/developer-tools/cli/#particle-compile), cpp files will need `#include "Particle.h"`.

1. Go to `~/git/device-os/main`
2. Run `make all PLATFORM=boron APPDIR=../../blinkled`

    or whereever you have the src for the app

    The result is located at `~/git/device-os/modules/blinkled/target/blinkled.bin`

3. Put the device in DFU mode by

    It is described [here](https://docs.particle.io/tutorials/device-os/led/photon/#dfu-mode-device-firmware-upgrade-)

4. flash it
