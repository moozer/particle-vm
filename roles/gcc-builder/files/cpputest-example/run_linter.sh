#!/usr/bin/env bash

set -e

cppcheck --enable=all --error-exitcode=1 --inline-suppr *.cpp
