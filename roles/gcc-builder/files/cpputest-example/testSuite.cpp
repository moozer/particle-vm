#include <CppUTest/TestHarness.h>

#include "CBasicMath.h"

TEST_GROUP(testSuite)
{

  CBasicMath* mTestObj;

  void setup()
  {
    mTestObj = new CBasicMath();
    mTestObj->Addition(2,3);
  }

  void teardown()
  {
    delete mTestObj;
  }
};

TEST(testSuite, testAddition)
{
  CHECK_EQUAL(5, mTestObj->Addition(2,3));
}

TEST(testSuite, testMultiply)
{
  CHECK_EQUAL(6, mTestObj->Multiply(2,3));
}
