gcc-builder
===============

This role make the target a gcc build host, ie. installs the neesary tools to build c and cpp programs using gcc.

It also installs libraries to do unittesting using cppunit.

A template project is in

Links:
* [cpputest homepage](https://github.com/cpputest/cpputest)
* [manual](https://cpputest.github.io/manual.html)
* [more stuff](https://www.sparkpost.com/blog/getting-started-cpputest/)
